# Exam 2 Lectures

## The indispensable One (Myth of George Washington)

Where does the myth come from? Weem's The Life of Washington (80 editions)

Weem had previously written how-to manuals, so Washington's biography becomes a how-to manual for citizens.

Cincinnatus: leaves his plow, directs army to victory, goes back to his plow. A hero to the founders. A republican hero, willing to sacrifice for the commonwealth.

Washington becomes an american Cincinnatus.

Story of cherry tree gets serialized in McGuffey readers, and from there it's stuck.

## The Man Behind the Myth
 - Big guys (6'2" - 6'4"), full foot taller than James Madison. Strong. Recognized as the best horseman in Virginia (big deal).	Commanding presence. Rather vane, he wanted to look good all the time. Complained his clothes never fit properly and he really cared about that.
 - Upper middle class, by his late 20's marries a widow (rich) and gets pretty rich. Tons of land, many plantations. Few hundered slaves.
 - World figure at age 25.
 - Wasn't very learned. Didn't really go to school, educated himself. Always felt he wasn't equal to other founders.
 - Volcanic temper. If you crossed him and he lost it, he'd go after you. He tried to keep it in, but couldn't always.
 - Popular in Virginia, house of Burgess. Gets elected as a deligate.
 - Shows up to contintal congress in full military uniform and volunteers to be general. Not unanimously selected, but he was the obvious choice.

## The Travails of Washington
 - Had to build an army from ground zero. Difficult to get soldiers, difficult to keep soldiers.
 - Problems with Congress. Lots of fighting between state deligates. Always writing congress for money and supplies.
 - Other competing generals.
 - Temporary enlistments (soldiers). Soldiers from different states varied greatly in the enlistment time. Some as short as 6 weeks. Typical was 3 months, 6 months was a long time. Constantly training recruits. Gets better over war time, but never completely solved.
 - Uncertain supply chain. Most merchants preferred to trade with the British since British money was circulated. Had to buy inferior material. Couldn't get what he needed.
 - Unprepared army.

## How Did He Do It?

Military
 - Changed the rules of war. Sort of goes "geurilla". Couldn't compete in large battles, so he turns to short quick fights. Get in get out. He doesn't have to win, he just has to survive. Eventually British will get sick of paying for the war.
 - Better training.
 - Revered by soldiers, he treated them fairly. He stayed with them at all times, same conditions as soldiers.
 - Washington's style of warfare is used by all kinds of revolutionary wars, even still today.

Politically
 - Undnerrated. He had been a politician elected several times. He knew people and politics. He knew power.
 - Experienced and street smart. Social standing.
 - He was able to keep tabs on rival generals.
 - Held the confidence of the people. Not just his army, but eventually all americans. This is when we changed from colonists to American citizens By the end of the war he is the most popular American.

Spiritually (the soul, no the holy spirit)
 - Believer in republican tradition. Seeks excellence. Maybe the best example of Plato's integrated soul.
 - Believer in commonwealth. Sees corruption in court.

## Video clip: Officers are planning to march on Phillidelphia because they haven't been paid. Washington finds out.
 - Washington was a drama guy, liked plays.
 - Be true to yourself and you won't march on government.
